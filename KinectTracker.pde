import java.util.ArrayList;
  
color drawColor;

class KinectTracker 
{
  // Depth threshold
  int threshold = 900;
  int threshold2 = 700;
  
  // Raw location
  PVector loc;
  
  // Interpolated location
  PVector lerpedLoc;
  
  // Depth data
  int[] depth;
  
  // What we'll show the user
  PImage display;
  PImage blurring_image;
  PImage dummy;

  color personColor;
  color black;
  
  KinectTracker() {
    kinect.initDepth();
    kinect.enableMirror(true);
    // Make a blank image
    display = createImage(kinect.width, kinect.height, ARGB);
    display2 = createImage(kinect.width, kinect.height, ARGB);
    blurring_image = createImage(kinect.width, kinect.height, ARGB);
    dummy = createImage(0,0,RGB);
    drawColor = color(50, 150, 0);
    personColor = color(150, 50, 50, 0);
    black = color(0,0,0);
    // Set up the vectors
    loc = new PVector(0, 0);
    lerpedLoc = new PVector(0, 0);
  }
  
  void setDrawColor(color newColor){
   drawColor = newColor; 
  }
  
  void track() {
    // Get the raw depth as array of integers
    depth = kinect.getRawDepth();
    // Being overly cautious here
    if (depth == null) return;
    float sumX = 0;
    float sumY = 0;
    float count = 0;
    for (int x = 0; x < kinect.width; x++) {
      for (int y = 0; y < kinect.height; y++) {
        int offset =  x + y*kinect.width;
        // Grabbing the raw depth
        int rawDepth = depth[offset];
        // Testing against threshold
        if (rawDepth < threshold) {
          sumX += x;
          sumY += y;
          count++;
        }
      }
    }
    // As long as we found something
    if (count != 0) {
      loc = new PVector(sumX/count, sumY/count);
    }
  
    // Interpolating the location, doing it arbitrarily for now
    lerpedLoc.x = PApplet.lerp(lerpedLoc.x, loc.x, 0.3f);
    lerpedLoc.y = PApplet.lerp(lerpedLoc.y, loc.y, 0.3f);
  }
  
  PVector getLerpedPos() {
    return lerpedLoc;
  }
  
  PVector getPos() {
    return loc;
  }
  
  void hasOneRedNeighbor(PImage display, PImage display2, int x, int y){
    if(x == 0 || y == 0 || x == display.width || y == display.height){
      return;
    }
    int pix = x + y * display.width;
    int up = x + (y+1)*display.width;
    int down = x + (y-1)*display.width;
    int left = x - 1 + y*display.width;
    int right = x + 1 + y*display.width;
    if(display.pixels[pix] == personColor){
      if(display.pixels[up] == personColor && display.pixels[down] == black && 
          display.pixels[left] == black && display.pixels[right] == black){
        display2.pixels[left] = personColor;
        display2.pixels[right] = personColor;
      }
      else if (display.pixels[up] == black && display.pixels[down] == personColor && 
          display.pixels[left] == black && display.pixels[right] == black){
        display2.pixels[left] = personColor;
        display2.pixels[right] = personColor;
      }
      else if (display.pixels[up] == black && display.pixels[down] == black && 
          display.pixels[left] == personColor && display.pixels[right] == black){
        display2.pixels[up] = personColor;
        display2.pixels[down] = personColor;
      }
      else if(display.pixels[up] == black && display.pixels[down] == black && 
          display.pixels[left] == black && display.pixels[right] == personColor){
        display2.pixels[up] = personColor;
        display2.pixels[down] = personColor;
      }
      display2.pixels[pix] = personColor;
    }
    else if (display2.pixels[pix] != personColor){
      display2.pixels[pix] = display.pixels[pix];
    }
  }
  
  PImage display() {
    PImage img = kinect.getDepthImage();
    //List of Colors you can draw with
    ArrayList<Object> drawColors = new ArrayList<Object>();
    drawColors.add(color(255, 153, 51));
    drawColors.add(color(255, 255, 51));
    drawColors.add(color(51, 255, 255));
    drawColors.add(color(50, 150, 0));
    drawColors.add(color(153, 51, 255));
    
    // Being cautious here
    if (depth == null || img == null) return img;
  
    // Rewrite the depth image to show which pixels are in threshold
    // second threshhold is where we draw
    display.loadPixels();
    display2.loadPixels();
    blurring_image.loadPixels();
    for (int x = 0; x < kinect.width; x++) {
      for (int y = 0; y < kinect.height; y++) {
        int offset = x + y * kinect.width;
        // Raw depth
        int rawDepth = depth[offset];
        int pix = x + y * display.width;
        if(x > kinect.width - 5 || y > kinect.height - 5 || x < 5 || y < 5){
          display.pixels[pix] = color(0, 0, 0);
        }
        else if (rawDepth < threshold && rawDepth >= threshold2) {
          // A red color instead
          if (drawColors.contains(display2.pixels[pix])) {
            display.pixels[pix] = display2.pixels[pix];
          } else {
              display.pixels[pix] = personColor;
          }
        } else if (rawDepth < threshold2&& rawDepth > 0) {
          if (!eraser)display2.pixels[pix] = drawColor;
          else display2.pixels[pix] = color(0, 0, 0);
        } else {
          if (drawColors.contains(display2.pixels[pix]) ) {
            display.pixels[pix] = display2.pixels[pix];
          } else {
            display.pixels[pix] = color(0, 0, 0);
          }
        }
      }
    }
//    for (int x = 0; x < kinect.width; x++) {
//      for (int y = 0; y < kinect.height; y++) {
//        int pix = x + y * display.width;
//        blurring_image.pixels[pix] = black;
//        hasOneRedNeighbor(display, blurring_image, x, y);
//      }
//    }
    blurring_image.updatePixels();
    display.updatePixels();
    display2.updatePixels();
    // Draw the image
    image(display, 0, 0);
    return display;
  }
  
  int getThreshold() {
    return threshold;
  }
  
  void setThreshold(int t) {
    threshold =  t;
  }
}

