When running, press "s" if Kinect is not recognized initially. Press "r" to turn off and on raining shapes. Press "e" to enter or exit erase mode. 
Press "c" to clear the screen of drawings. Alternatively can be controlled from the GUI. 

Reaching within a certain depth threshold will cause you to draw in the selected color or to erase when selected. 
Turn on the shape rain for shapes to fall that you can interact with. 
Run in Processing version 2.2.1 with the libraries mentioned below by storing files in a folder named interactiveSketchbook_Main and opening
the .pde file of the same name. 

Works Cited:
Ahlquist, Sean. "Tactile Technologies for Play and Learning." Ann Arbor. 14 Sept. 2015. EECS 481 Lecture.

Borenstein, Greg. OpenCV for Processing Library. 20 Apr. 2015. Web. 3 Nov. 2015. Retrieved from https://github.com/atduskgreg/opencv-processing  

Chen, Chloe. "Kinect Silhouette and Kinect Dot Cam Using Processing." ChloeChen. 13 Sept. 2014. Web. 28 Oct. 2015. Retrieved from http://www.chloechen.io/kinect-silhouette-and-kinect-stipple-cam-using-processing/ 

Gachadoat, Julien and Klingemann, Mario. BlobDetection Library and Examples (including “Super Fast Blur” functionality). Oct. 2012. Web. 3 Nov. 2015. Retrieved from http://www.v3ga.net/processing/BlobDetection/index-page-download.html 

Lager, Peter. GUI for Processing (G4P) Library and Examples. 10 Dec. 2015. Web. 13 Dec. 2015. Retrieved from http://www.lagers.org.uk/g4p/download.html

McMeeking, Chris. “Kinect Color App.” Source Code for Coloring Wall. Ann Arbor. EECS 481 Project at the University of Michigan.

Owed, Amnon. "Kinect Physics Tutorial for Processing." Creative Applications Network. 17 Sept. 2012. Web. 25 Oct. 2015. Retrieved from http://www.creativeapplications.net/processing/kinect-physics-tutorial-for-processing/ 

Schmidt, Karsten. ToxicLib Library. 3 Jan. 2011. Web. 3 Nov. 2015. Retrieved from https://bitbucket.org/postspectacular/toxiclibs/downloads/ 

Shiffman, Daniel. Box2D for Processing: Library and Examples. 18 Oct. 2015. Web. 3 Nov. 2015. Retrieved from https://github.com/shiffman/Box2D-for-Processing 

Shiffman, Daniel. Open Kinect for Processing: Library, Demos and Examples. Web. 3 Nov. 2015. Retrieved from http://shiffman.net/p5/kinect/ (library downloaded through Processing Interface)

Snyder, Benn. OpenKinect libfreenect Library. 18 Oct. 2015. Web. 3 Nov. 2015. Retrieved from https://github.com/OpenKinect/libfreenect 

Soloman, Onna. "The Play Project." Ann Arbor. Sept. 2015. EECS 481 Lecture.

	
