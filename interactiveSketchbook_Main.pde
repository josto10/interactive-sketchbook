// import libraries
import processing.opengl.*; // opengl
import blobDetection.*; // blobs
import toxi.geom.*; // toxiclibs shapes and vectors
import toxi.processing.*; // toxiclibs display
import shiffman.box2d.*;// shiffman's jbox2d helper library
import org.jbox2d.collision.shapes.*; // jbox2d
import org.jbox2d.common.*; // jbox2d
import org.openkinect.freenect.*;
import org.openkinect.processing.*;
import org.jbox2d.dynamics.*; // jbox2d
import java.awt.Polygon;//for creating the polygons
import java.util.*;//java util 
import g4p_controls.*;//G4P GUI for Processing Library


//=================================
//Variable Definitions
//=================================
//Create kinect and image objects
Kinect kinect;
PImage display2;
KinectTracker tracker;

//initialize boolean flags
boolean eraser = false;
boolean rainShapes = false;

// declare BlobDetection object
BlobDetection theBlobDetection;

// Objects for creating polygons
ToxiclibsSupport gfx;
PolygonBlob poly;
ArrayList<PolygonBlob> polygonBodies = new ArrayList<PolygonBlob>();

// PImage for blob detection
PImage blobs;
// the kinect's dimensions 
int kinectWidth = 640;
int kinectHeight = 480;
// to center and rescale from 640x480 to higher custom resolutions
float reScale;

// background and blob color
color bgColor, blobColor;
// three color palettes (Amnon Owed)
String[] palettes = {
  "-1117720,-13683658,-8410437,-9998215,-1849945,-5517090,-4250587,-14178341,-5804972,-3498634", 
  "-67879,-9633503,-8858441,-144382,-4996094,-16604779,-588031", 
  "-1978728,-724510,-15131349,-13932461,-4741770,-9232823,-3195858,-8989771,-2850983,-10314372"
};
color[] colorPalette;

// the main Box2D object for physics
Box2DProcessing box2d;
// list to hold the circles and polygons for the shape rain
ArrayList<CustomShape> polygons = new ArrayList<CustomShape>();

//user interface objects
GWindow gWin;
GButton btnInit, btnRain, btnClear, btnErase, btnTroubleshoot, btnGreen, btnYellow, btnBlue, btnPurple, btnOrange;

//=================================
//Setup/initializations
//=================================
void setup() {
  // custom size
  size(1280, 720, OPENGL);
  
  kinect = new Kinect(this);
  kinect.initDepth();
  kinect.initVideo();
  kinect.enableMirror(true);
  tracker = new KinectTracker();
  kinect.enableColorDepth(false);
  reScale = (float) width / kinectWidth;
  
  // create a smaller blob image for speed and efficiency
  blobs = createImage(kinectWidth/4, kinectHeight/4, RGB);
  // initialize blob detection object to the blob image dimensions
  theBlobDetection = new BlobDetection(blobs.width, blobs.height);
  theBlobDetection.setThreshold(0.2);
  // initialize ToxiclibsSupport object
  gfx = new ToxiclibsSupport(this);
  
  // setup box2d, create world, set gravity
  box2d = new Box2DProcessing(this);
  box2d.createWorld();
  box2d.setGravity(0, -30);
  
  // set colors (background, blob)
  setRandomColors(1);
  bgColor = color(0, 0, 0);
  blobColor = color(150, 50, 50);
  
  //USER INTERFACE
  initInterface();

}

//=================================
//Key Controls for Project
//=================================
void keyPressed() {
  //restart kinect
  if (key == 's') {
    kinect = new Kinect(this);
    kinect.initDepth();
    kinect.initVideo();
    kinect.enableMirror(true);
    //kinect.enableIR(ir);
    kinect.enableColorDepth(false);
  }
  //eraser mode on/off
  if (key == 'e') {
    if (!eraser)eraser = true;
    else eraser = false;
  }
  //shape rain mode on/off
  if (key == 'r') {
    if (!rainShapes)rainShapes = true;
    else rainShapes = false;
  }
  //clear the screen
  if (key == 'c') {
    display2 = createImage(kinect.width, kinect.height, RGB);
  }
}


//========================================
//Draw function, calculate blobs/polygons
//========================================
void draw() {
  background(bgColor);
  //get silhouette
  tracker.track();
  //find the blobs from depth display from tracker
  blobs.copy(tracker.display(), 0, 0, kinect.width, kinect.height, 0, 0, blobs.width, blobs.height);
  // blur the blob image
  blobs.filter(BLUR, 1.5);
  // detect the blobs
  theBlobDetection.computeBlobs(blobs.pixels);
  
  // create and store the polygons from the blobs
  for (int n=0 ; n<theBlobDetection.getBlobNb(); n++) {
        // initialize a new polygon
        poly = new PolygonBlob();
        poly.createPolygon(theBlobDetection.getBlob(n));
        // create the box2d body from the polygon
        try{
          poly.createBody();
        }
        catch (Exception e){
          //System.out.println("caught it!!");
        }
        polygonBodies.add(poly);
  }
  
  // update and display everything
  updateAndDrawBox2D();
  tracker.display();

  //if shape rain is on, display them. otherwise, get rid of them
  if (rainShapes) {
    for (int i=polygons.size ()-1; i>=0; i--) {
      CustomShape cs = polygons.get(i);
      // if the shape is off-screen remove it 
      if (cs.done()) {
        polygons.remove(i);
        // otherwise update and display 
      } else {
        cs.update();
        cs.display();
      }
    }
  }else{
   polygons.clear(); 
  }  
  
  // destroy the person's body (important!)
  for(PolygonBlob polyBlob : polygonBodies){
      polyBlob.destroyBody();
  }
  polygonBodies.clear();
}


//========================================
//Update and Draw polygons
//========================================
void updateAndDrawBox2D() 
{
  // if frameRate is sufficient, add a polygon and a circle with a random radius
  if (frameRate > 20) {
    polygons.add(new CustomShape(kinectWidth/2, -50, -1));
    polygons.add(new CustomShape(kinectWidth/2, -50, random(2.5, 10)));
  }
  // take one step in the box2d physics world
  box2d.step();

  // center and reScale from Kinect to custom dimensions
  translate(0, (height-kinectHeight*reScale)/2);
  scale(reScale);

  // display the person's polygon  
  for(PolygonBlob polyBlob : polygonBodies){
     noStroke();
    fill(blobColor);
    gfx.polygon2D(polyBlob);
  } 
}

//========================================
//Sets the colors every nth frame
//========================================
void setRandomColors(int nthFrame) {
  if (frameCount % nthFrame == 0) {
    // turn a palette into a series of strings
    String[] paletteStrings = split(palettes[int(random(palettes.length))], ",");
    // turn strings into colors
    colorPalette = new color[paletteStrings.length];
    for (int i=0; i<paletteStrings.length; i++) {
      colorPalette[i] = int(paletteStrings[i]);
    }
    // set all shape colors randomly
    for (CustomShape cs : polygons) { 
      cs.col = getRandomColor();
    }
  }
}

//========================================
// returns a random color from the palette 
//=========================================
color getRandomColor() {
  return colorPalette[int(random(1, colorPalette.length))];
}

//=================
//USER INTERFACE
//=================
ArrayList<GButton> colorButtons;
GButton lastColor;

public void initInterface(){
  //Disable std out messages
  G4P.messagesEnabled(false);
  colorButtons = new ArrayList<GButton>();
  int yvalue = 10;//y position
  int xvalue = 10;//x position
  int xwidth = 120;//width
  int yheight = 60;//height
  int xIncr = xwidth + 5;//find next position
  //Main Window of Interface
  gWin = new GWindow(this, "Control Panel", 100, 100, 520, 150, false, JAVA2D);
  
  //First trouble shoot button for kinect, to add a layer between a user and potentially crashing the program
  btnTroubleshoot = new GButton(gWin.papplet, xvalue, yvalue, 120, 60, "Click Here if Display Screen is Black");
  btnTroubleshoot.addEventHandler(this, "btnTroubleshootAction");
  btnTroubleshoot.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  btnTroubleshoot.setTextBold();
  
  //Button to reset the kinect, hidden at first
  btnInit = new GButton(gWin.papplet, xvalue, yvalue, 120, 60, "Click to Retry Kinect");
  btnInit.addEventHandler(this, "btnInitKinect");
  btnInit.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  btnInit.setVisible(false);
  btnInit.setEnabled(false);
  btnInit.setTextBold();
  
  //button to toggle shape rain
  btnRain = new GButton(gWin.papplet, xvalue+xIncr, yvalue, 120, 60, "Turn Shape Rain On");
  btnRain.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  btnRain.addEventHandler(this, "btnToggleRain");
  btnRain.setTextBold();
  
  //button to clear the screen
  btnClear = new GButton(gWin.papplet, xvalue+xIncr*2, yvalue, 120, 60, "Clear Screen");
  btnClear.addEventHandler(this, "btnClearScreen");
  btnClear.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  btnClear.setTextBold();
  
  //button to toggle eraser
  btnErase = new GButton(gWin.papplet, xvalue+xIncr*3, yvalue, 120, 60, "Turn Eraser On");
  btnErase.addEventHandler(this, "btnToggleEraser");
  btnErase.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  btnErase.setTextBold();
  colorButtons.add(btnErase);
  
  //Color choosing buttons
  yvalue += 65;
  xwidth = 95;
  xIncr = xwidth + 5;
  //Orange
  btnOrange = new GButton(gWin.papplet, xvalue, yvalue, xwidth, yheight, "Orange");
  btnOrange.addEventHandler(this, "btnToggleOrange");
  btnOrange.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  btnOrange.setTextBold();
  colorButtons.add(btnOrange);
  
  //Yellow
  btnYellow = new GButton(gWin.papplet, xvalue+xIncr, yvalue, xwidth,yheight, "Yellow");
  btnYellow.addEventHandler(this, "btnToggleYellow");
  btnYellow.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  btnYellow.setTextBold();
  colorButtons.add(btnYellow);
  
  //Green
  btnGreen= new GButton(gWin.papplet, xvalue+xIncr*2, yvalue, xwidth, yheight, "Green");
  btnGreen.addEventHandler(this, "btnToggleGreen");
  btnGreen.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  btnGreen.setTextBold();
  colorButtons.add(btnGreen);
  lastColor = btnGreen;
  
  //Blue
  btnBlue = new GButton(gWin.papplet, xvalue+xIncr*3, yvalue, xwidth, yheight, "Blue");
  btnBlue.addEventHandler(this, "btnToggleBlue");
  btnBlue.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  btnBlue.setTextBold();
  colorButtons.add(btnBlue);
  
  //Purple
  btnPurple = new GButton(gWin.papplet, 10+xIncr*4, yvalue, xwidth, yheight, "Purple");
  btnPurple.addEventHandler(this, "btnTogglePurple");
  btnPurple.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  btnPurple.setTextBold();
  colorButtons.add(btnPurple);
}

//========================================
//Actions for Buttons when they are clicked
//========================================
void btnTroubleshootAction(GButton source, GEvent event) {
  btnTroubleshoot.setVisible(false);
  btnTroubleshoot.setEnabled(false);
  btnInit.setEnabled(true);
  btnInit.setVisible(true);
}

 void btnInitKinect(GButton source, GEvent event) {
    kinect = new Kinect(this);
    kinect.initDepth();
    kinect.initVideo();
    kinect.enableMirror(true);
    //kinect.enableIR(ir);
    kinect.enableColorDepth(false); 
    //Disable button
    btnInit.setEnabled(false);
    btnInit.setText("Kinect Enabled");
    btnInit.setLocalColorScheme(GCScheme.RED_SCHEME);
}

void btnToggleRain(GButton source, GEvent event){
  if (!rainShapes){
    rainShapes = true;
    btnRain.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
    btnRain.setText("Turn Shape Rain Off");
    btnRain.setTextBold();
  }
    else{
      rainShapes = false;
      btnRain.setLocalColorScheme(GCScheme.GREEN_SCHEME);
      btnRain.setText("Turn Shape Rain On");
      btnRain.setTextBold();
    }
}

void btnClearScreen(GButton source, GEvent event){
  display2 = createImage(kinect.width, kinect.height, RGB);
}

void btnToggleEraser(GButton source, GEvent event){
    if (!eraser){
      colorButtonDefaultBehavior();
      eraser = true;
      btnErase.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
      btnErase.setText("Turn Eraser Off");
      btnErase.setTextBold();
    }
    else{
      eraser = false;
      lastColor.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
      btnErase.setLocalColorScheme(GCScheme.GREEN_SCHEME);
      btnErase.setText("Turn Eraser On");
      btnErase.setTextBold();
    }
}

//If a color button is selected, change all other buttons to green and turn off eraser
void colorButtonDefaultBehavior(){
 eraser = false;
 btnErase.setText("Turn Eraser On");
  for(GButton btn : colorButtons)btn.setLocalColorScheme(GCScheme.GREEN_SCHEME);
}

void btnToggleOrange(GButton source, GEvent event){
  colorButtonDefaultBehavior();
  btnOrange.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  lastColor = btnOrange;
  tracker.setDrawColor(color(255, 153, 51)); 
}  

void btnToggleYellow(GButton source, GEvent event){
  tracker.setDrawColor(color(255, 255, 51)); 
  colorButtonDefaultBehavior();
  lastColor = btnYellow;
  btnYellow.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
} 

void btnToggleGreen(GButton source, GEvent event){
  tracker.setDrawColor(color(50, 150, 0)); 
  colorButtonDefaultBehavior();
  lastColor = btnGreen;
  btnGreen.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
} 

void btnToggleBlue(GButton source, GEvent event){
  tracker.setDrawColor(color(51, 255, 255)); 
  colorButtonDefaultBehavior();
  lastColor = btnBlue;
  btnBlue.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
} 

void btnTogglePurple(GButton source, GEvent event){
  tracker.setDrawColor(color(153, 51, 255)); 
  colorButtonDefaultBehavior();
  lastColor = btnPurple;
  btnPurple.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
} 


